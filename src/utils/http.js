import axios from 'axios'

//创建axios实例对象
let instance = axios.create({
  baseURL: process.env.VUE_APP_URL, // 服务器地址
  timeout: 10 * 1000, // 超时时间
  headers: {
    'Content-Type': 'application/json; charset=utf-8',
  },
})
// 请求拦截
instance.interceptors.request.use(
  (config) => {
    // 请求参数
    return config
  },
  (error) => {
    // 对请求错误做些什么
    return Promise.reject(error)
  },
)

// 响应拦截
instance.interceptors.response.use(
  (data) => {
    // code == 600 说明token过期,需要重新登录
    return Promise.resolve(data)
  },
  (error) => {
    return Promise.reject(error)
  },
)
const http = {
  get(url, params) {
    return instance.get(url, { params })
  },
  post(url, data) {
    return instance.post(url, data)
  },
}
export default http
