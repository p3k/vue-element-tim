// 手机号正则
const phoneReg = (value) => {
  return /^1[3456789]\d{9}$/.test(value)
}

// 数据处理
const handleData = (value) => {
  if (typeof value.payload.data !== 'string') return value
  value.payload.data = JSON.parse(value.payload.data)
  return value
}

module.exports = {
  phoneReg,
  handleData,
}
