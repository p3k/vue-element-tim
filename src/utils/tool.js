import moment from 'moment'
import Vue from 'vue'

Vue.filter('dateFormat', function(date, formatStr = 'YYYY-MM-DD HH:mm') {
  return moment(date).format(formatStr)
})
