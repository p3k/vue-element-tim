export const Demo = {
  beforeCreate() {
    console.log('mixins----------beforeCreate')
  },
  created() {
    console.log('mixins----------created')
  },
  beforeMount() {
    console.log('mixins----------beforeMount')
  },
  mounted() {
    console.log('mixins----------mounted')
  },
  beforeUpdate() {
    console.log('mixins----------beforeUpdate')
  },
  updated() {
    console.log('mixins----------updated')
  },
  beforeDestroy() {
    console.log('mixins----------beforeDestroy')
  },
  destroyed() {
    console.log('mixins----------destroyed')
  },
}
