import http from '@/api/index'
import '@/assets/css/global.css'
import '@/assets/scss/index.scss'
import highcharts from 'highcharts'
import highcharts3d from 'highcharts/highcharts-3d'
// import 'lib-flexible'
import Vue from 'vue'
import App from './App.vue'
import './plugin/element'
import './plugin/vant'
import router from './router'
import store from './store'
import { IMClass } from './tim/im.js'
import './utils/tool.js'
import Message from '@/components/common/extend/index.js'
Vue.prototype.$toast = Message.install
highcharts3d(highcharts)

Vue.prototype.$http = http
// im初始化
Vue.prototype.$IM = new IMClass()
Vue.prototype.$IM.create()
// event Bus 用于无关系组件间的通信。
Vue.prototype.$bus = new Vue()
Vue.config.productionTip = false
console.log(process.env)
new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app')
