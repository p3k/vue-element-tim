import {
  Form,
  FormItem,
  Button,
  Loading,
  Message,
  MessageBox,
  Select,
  Option,
  Input,
  Row,
  Col,
  Tooltip,
  Popover,
  Dropdown,
  DropdownMenu,
  DropdownItem,
  Tabs,
  TabPane,
  Progress,
  Carousel,
  CarouselItem,
  Tag,
  Dialog,
  Upload,
  Table,
  TableColumn,
  Switch,
} from 'element-ui'
import Vue from 'vue'
Vue.use(Form)
Vue.use(FormItem)
Vue.use(Button)
Vue.use(Select)
Vue.use(Option)
Vue.use(Input)
Vue.use(Loading)
Vue.use(Row)
Vue.use(Col)
Vue.use(Tooltip)
Vue.use(Popover)
Vue.use(Dropdown)
Vue.use(DropdownMenu)
Vue.use(DropdownItem)
Vue.use(Tabs)
Vue.use(TabPane)
Vue.use(Progress)
Vue.use(Carousel)
Vue.use(CarouselItem)
Vue.use(Tag)
Vue.use(Dialog)
Vue.use(Upload)
Vue.use(Table)
Vue.use(TableColumn)
Vue.use(Switch)

Vue.prototype.$loading = Loading.service
Vue.prototype.$alert = MessageBox.alert
Vue.prototype.$confirm = MessageBox.confirm
Vue.prototype.$prompt = MessageBox.prompt
Vue.prototype.$message = Message
