import { Swipe, SwipeItem } from 'vant'
import Vue from 'vue'

Vue.use(Swipe)
Vue.use(SwipeItem)
