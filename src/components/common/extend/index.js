import Vue from 'vue'
import Message from './message.vue'
const MessageBox = Vue.extend(Message)

let instances = []
let seed = 1
Message.install = function(content, type) {
  let options
  if (content) {
    options = {
      content,
      type: type || 'success',
    }
  } else {
    options = {
      content: '',
    }
  }
  let userOnClose = options.onClose
  let id = 'message_' + seed++
  options.onClose = function() {
    instance.close(id, userOnClose)
  }
  let instance = new MessageBox({
    data: options,
  }).$mount()

  console.log(instance, '===')
  document.body.appendChild(instance.$el)

  Vue.nextTick(() => {
    let verticalOffset = options.offset || 20
    instances.forEach((item) => {
      verticalOffset += item.$el.offsetHeight + 16
    })
    instance.verticalOffset = verticalOffset
    instance.visible = true
    instances.push(instance)
  })
  instance.close = function(id, userOnClose) {
    console.log('触发了==')
    let len = instances.length
    let index = -1
    let removedHeight
    for (let i = 0; i < len; i++) {
      if (id === instances[i].id) {
        removedHeight = instances[i].$el.offsetHeight
        index = i
        if (typeof userOnClose === 'function') {
          userOnClose(instances[i])
        }
        instances.splice(i, 1)
        break
      }
    }
    if (len <= 1 || index === -1 || index > instances.length - 1) return
    for (let i = index; i < len - 1; i++) {
      let dom = instances[i].$el
      dom.style['top'] =
        parseInt(dom.style['top'], 10) - removedHeight - 16 + 'px'
    }
  }
}
export default Message
