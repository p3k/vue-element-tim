const state = {
  // 是否登录
  isLogin: false,
  // im是否ready
  isSDKReady: false,
  // 用户id
  userID: '',
  // 客服ID
  toID: '',
  // tabs
  tabIndex: 0,
  // 网页携带id
  urlId: '',
  // 网页携带,是否需要登录 0:登录, 1:不登录
  isSwitch: null,
  // 是否是机器人
  isRobot: false,
  // 客服信息
  employeeInfo: {},
  // 客服标签
  employeeTagsList: [],
  // 企业信息
  companyInfo: {},
  // 问题列表
  questionList: [],
  // 轮播图
  advertising: [],
  // 线索
  referrer: '',
}
const mutations = {
  // 用户id
  setUserID(state, userID) {
    state.userID = userID
  },
  // 客服id
  setToID(state, toID) {
    state.toID = toID
  },
  // 线索id
  setReferrer(state, referrer) {
    state.referrer = referrer
  },
  // 保存客服信息
  setEmployeeInfo(state, info) {
    state.employeeInfo = info
  },
  // 保存客服标签
  setEmployeeTagsList(state, data) {
    state.employeeTagsList = data
  },
  // 保存企业信息
  setCompanyInfo(state, info) {
    state.companyInfo = info
  },
  // 轮播图
  setAdvertising(state, data) {
    state.advertising = data || []
  },
  // 保存问题列表数据
  setQuestionList(state, data) {
    state.questionList = data || []
  },
  // 是否为机器人
  toggleIsRobot(state, isRobot) {
    state.isRobot = isRobot
  },
  // 保存网页携带id
  setUrlID(state, urlId) {
    state.urlId = urlId
  },
  // 是否需要登录
  setIsSwitch(state, isSwitch) {
    state.isSwitch = isSwitch
  },
  // 登录状态切换
  toggleIsLogin(state, isLogin) {
    state.isLogin = typeof isLogin === 'undefined' ? !state.isLogin : isLogin
  },
  // im sdk状态切换
  toggleIsSDKReady(state, isSDKReady) {
    state.isSDKReady =
      typeof isSDKReady === 'undefined' ? !state.isSDKReady : isSDKReady
  },
  // tabs切换
  toggleIsTabs(state, index) {
    state.tabIndex = index
  },
  // 清除数据
  clear(state) {
    state.isLogin = false
    state.isSDKReady = false
    state.userID = ''
  },
}
const actions = {}
const getters = {}
export default {
  // 开启命名空间
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
}
