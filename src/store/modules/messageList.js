import TIM from 'tim-js-sdk-ws'
// import { handleData } from '@/utils/util'
const state = {
  // 客户信息
  messageList: [],
  // 当前会话消息列表是否已经请求完所有消息
  isCompleted: false,
  // 更新messageID，续拉时要用到
  nextReqMessageID: '',
  // 消息进来
  flowIn: false,
  // 消息提醒
  isPrompt: true,
  // 当前请求状态
  isRequest: false,
  // 新消息
  messageIsUpdate: true,
}
const mutations = {
  // 聊天消息
  setMessageList(state, e) {
    state.messageList = [...e.data.messageList, ...state.messageList]
    state.isCompleted = e.data.isCompleted
    state.nextReqMessageID = e.data.nextReqMessageID
    state.isRequest = false
  },
  // 请求状态切换
  toggleIsRequest(state, e) {
    state.isRequest = e
  },
  // 数据更新
  updateMessageList(state, data) {
    console.log(data, '数据更新')
    let index = state.messageList.findIndex((x) => x.ID === data.ID)
    if (index != -1) {
      state.messageList.splice(index, 1, data)
    } else {
      state.messageList = [...state.messageList, data]
      state.messageIsUpdate = !state.messageIsUpdate
    }
    // 消息提醒关闭状态,结束后面流程
    if (!state.isPrompt) return
    // 触发新消息提醒
    if (data.flow === 'in') {
      state.flowIn = !state.flowIn
    }
  },
  // 清除数据
  clear(state) {
    state.messageList = []
    state.isCompleted = false
    state.nextReqMessageID = ''
    state.flowIn = false
    state.isRequest = false
  },
  // 消息提示音状态切换
  toggleIsPrompt(state) {
    state.isPrompt = !state.isPrompt
  },
}
const actions = {}
const getters = {
  // 用于当前会话的图片预览
  imgUrlList: (state) => {
    return state.messageList
      .filter(
        (message) => message.type === TIM.TYPES.MSG_IMAGE && !message.isRevoked,
      ) // 筛选出没有撤回并且类型是图片类型的消息
      .map((message) => message.payload.imageInfoArray[0].url)
  },
}
export default {
  // 开启命名空间
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
}
