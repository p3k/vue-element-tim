export default {
  isLogin: (state) => state.user.isLogin,
  isSDKReady: (state) => state.user.isSDKReady,
  userID: (state) => state.user.userID,
  toID: (state) => state.user.toID,
  tabIndex: (state) => state.user.tabIndex,
  urlId: (state) => state.user.urlId,
  isSwitch: (state) => state.user.isSwitch,
  isRobot: (state) => state.user.isRobot,
  employeeInfo: (state) => state.user.employeeInfo,
  employeeTagsList: (state) => state.user.employeeTagsList,
  companyInfo: (state) => state.user.companyInfo,
  questionList: (state) => state.user.questionList,
  advertising: (state) => state.user.advertising,
  referrer: (state) => state.user.referrer,
  messageList: (state) => state.messageList.messageList,
  isCompleted: (state) => state.messageList.isCompleted,
  nextReqMessageID: (state) => state.messageList.nextReqMessageID,
  flowIn: (state) => state.messageList.flowIn,
  isPrompt: (state) => state.messageList.isPrompt,
  messageIsUpdate: (state) => state.messageList.messageIsUpdate,
  isRequest: (state) => state.messageList.isRequest,
}
