import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/home',
    component: () => import('../views/Home.vue'),
  },
  {
    path: '/home',
    name: 'home',
    component: () => import('../views/Home.vue'),
  },
  {
    path: '/about',
    name: 'about',
    component: () => import('../views/About.vue'),
  },
  {
    path: '/map',
    name: 'map',
    component: () => import('../views/Map.vue'),
  },
  {
    path: '/auth',
    name: 'auth',
    component: () => import('../views/Auth.vue'),
  },
  {
    path: '/user/:id',
    component: () => import('../views/User.vue'),
    // props: true, id可以通过 user 页面的 props 获取
    props: true,
  },
  // 404page
  {
    path: '/404',
    name: 'notFound',
    component: () => import('../views/NotFound.vue'),
  },
  // 页面不存在的情况下会跳到404页面
  {
    path: '*',
    redirect: '/404',
    name: 'notFound',
    hidden: true,
  },
]

// BASE_URL 在 vue.config.js 里面有对应的 publicPath option, 同时 是你的应用部署的基本路径
const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
})

/**
 * to     即将要进入的目标
 * from   当前正要离开的路由
 * next   resolve 下一步, 必须调用
 */
router.beforeEach((to, from, next) => {
  // console.log(to, from)
  next()
})

export default router
