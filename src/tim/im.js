// 从v2.11.0起，SDK 支持了 WebSocket，推荐接入
import TIM from 'tim-js-sdk-ws'
// import TIM from 'tim-js-sdk'; // HTTP 版本
import TIMUploadPlugin from 'tim-upload-plugin'
import store from '../store'
export class IMClass {
  constructor() {
    this.tim = null
    this.sdkAppId = ''
  }

  create() {
    this.tim = TIM.create({
      SDKAppID: Number(process.env.VUE_APP_sdkAppId),
    })
    /**
     * 0: 普通级别，日志量较多，接入时建议使用
     * 1: release 级别，SDK 输出关键信息，生产环境时建议使用
     */
    this.tim.setLogLevel(1)
    this.tim.registerPlugin({ 'tim-upload-plugin': TIMUploadPlugin })
    this.initEvents()
  }

  initEvents() {
    this.tim.on(TIM.EVENT.ERROR, this.imErrorEvent, this)
    this.tim.on(TIM.EVENT.SDK_READY, this.onReady, this)
    this.tim.on(TIM.EVENT.GROUP_LIST_UPDATED, this.onGroupListUpdated, this)
    // 收到新消息
    this.tim.on(TIM.EVENT.MESSAGE_RECEIVED, this.onReceiveMessage)
    // this.tim.on(
    //   TIM.EVENT.CONVERSATION_LIST_UPDATED,
    //   this.onUpdateConversationList,
    //   this,
    // )
  }
  login(userID, userSig) {
    this.userID = userID
    this.userSig = userSig
    return new Promise((resolve, reject) => {
      this.tim
        .login({
          userID,
          userSig,
        })
        .then((res) => {
          console.log(
            `%cIM---登录成功`,
            'color: green;font-size:14px;font-weight: 700;',
            res,
          )
          if (!res.data.errorInfo) {
            store.commit('user/toggleIsLogin', true)
            store.commit('user/setUserID', userID)
            sessionStorage.setItem('userID', userID)
            // 自己的id
            localStorage.setItem('userID', userID)
            console.log(localStorage.getItem('userID'), '===')
          }
          resolve(res)
        })
        .catch((err) => {
          console.log(
            `%cIM---登录失败`,
            'color: red;font-size:14px;font-weight: 700;',
          )
          store.commit('user/toggleIsLogin', false)
          reject(err)
        })
    })
  }
  logout() {
    // this.tim.logout()
    return new Promise((resolve, reject) => {
      this.tim
        .logout()
        .then((res) => {
          console.log(
            `%cIM---退出登录`,
            'color: green;font-size:14px;font-weight: 700;',
            res,
          )
          store.commit('user/clear')
          store.commit('messageList/clear')
          resolve(res)
        })
        .catch((err) => {
          console.log(
            `%cIM---退出登录`,
            'color: red;font-size:14px;font-weight: 700;',
          )
          store.commit('user/clear')
          store.commit('messageList/clear')
          reject(err)
        })
    })
  }
  // onUpdateConversationList(data) {
  //   console.log(data)
  // }
  // 列表数据更新
  onReceiveMessage(e) {
    store.commit('messageList/updateMessageList', e.data[0])
  }
  imErrorEvent(e) {
    console.log(e)
  }

  createCustomMessage(message, description, to) {
    return this.tim.createCustomMessage({
      to: to,
      conversationType: TIM.TYPES.CONV_C2C,
      payload: {
        data: JSON.stringify(message),
        description,
      },
    })
  }

  onReady(e) {
    console.log(`%cIM---Ready`, 'color: green;font-size:14px;font-weight: 700;')
    const isSDKReady = e.name === TIM.EVENT.SDK_READY ? true : false
    store.commit('user/toggleIsSDKReady', isSDKReady)
    if (e.name === TIM.EVENT.SDK_READY) {
      this.getMessageList()
    }
    if (this.readyCb) {
      this.readyCb(e)
    }
  }
  setReadyCb(cb) {
    this.readyCb = cb
  }
  /**
   * 获取消息列表
   * 调用时机：打开某一会话时或下拉获取历史消息时
   * @param {String} conversationID
   */
  getMessageList() {
    if (
      store.state.messageList.isRequest ||
      store.state.messageList.isCompleted
    )
      return
    store.commit('messageList/toggleIsRequest', true)
    const { nextReqMessageID } = store.state.messageList
    const { toID } = store.state.user
    this.tim
      .getMessageList({
        conversationID: 'C2C' + toID,
        nextReqMessageID,
        count: 15,
      })
      .then((imReponse) => {
        store.commit('messageList/setMessageList', imReponse)
      })
      .catch((err) => {
        console.log(err, 'err================')
      })
  }
  onGroupListUpdated(e) {
    console.log(
      '🚀 ~ file: im.ts ~ line 154 ~ IMClass ~ onGroupListUpdated ~ e',
      e,
    )
    if (this.groupListUpdatedEventCb) this.groupListUpdatedEventCb(e)
  }

  sendMsg(msg) {
    // return this.tim.sendMessage(msg)
    return new Promise((resolve, reject) => {
      this.tim
        .sendMessage(msg)
        .then((res) => {
          console.log(res)
          resolve(res)
        })
        .catch((err) => {
          console.log(err)
          reject(err)
        })
    })
  }
  /**
   * @param  {string} message 要发送的信息
   * @param  {string} to 发送的对象
   * @description 创建 text 信息对象
   */
  createTextMessage(message, to) {
    return this.tim.createTextMessage({
      to,
      conversationType: TIM.TYPES.CONV_C2C,
      payload: {
        text: message,
      },
    })
  }
  /**
   * @param  {object} image 要发送的信息
   * @param  {string} to 发送的对象
   * @description 创建 image 信息对象
   */
  createImageMessage(image, to) {
    return (this.message = this.tim.createImageMessage({
      to,
      conversationType: TIM.TYPES.CONV_C2C,
      payload: {
        file: image,
      },
      onProgress: (percent) => {
        this.message.progress = percent
      },
    }))
  }
  /**
   * @param  {obejct} video 要发送的信息
   * @param  {string} to 发送的对象
   * @description 创建 video 信息对象
   */
  createVideoMessage(video, to) {
    return (this.message = this.tim.createVideoMessage({
      to,
      conversationType: TIM.TYPES.CONV_C2C,
      payload: {
        file: video,
      },
      onProgress: (percent) => {
        this.message.progress = percent
      },
    }))
  }
  /**
   * @param  {obejct} video 要发送的信息
   * @param  {string} to 发送的对象
   * @description 创建 file 信息对象
   */
  createFileMessage(file, to) {
    return (this.message = this.tim.createFileMessage({
      to,
      conversationType: TIM.TYPES.CONV_C2C,
      payload: {
        file: file,
      },
      onProgress: (percent) => {
        this.message.progress = percent
      },
    }))
  }
  /**
   * @param  {obejct} audio 要发送的信息
   * @param  {string} to 发送的对象
   * @description 创建 video 信息对象
   */
  createAudioMessage(audio, to) {
    return this.tim.createAudioMessage({
      to,
      conversationType: TIM.TYPES.CONV_C2C,
      /** cloud.tencent.com/document/product/269/3663#.E6.B6.88.E6.81.AF.E4.BC.98.E5.85.88.E7.BA.A7.E4.B8.8E.E9.A2.91.E7.8E.87.E6.8E.A7.E5.88.B6) */
      // 消息优先级，用于群聊（v2.4.2起支持）。如果某个群的消息超过了频率限制，后台会优先下发高优先级的消息，详细请参考：https:
      /**
       * 支持的枚举值：TIM.TYPES.MSG_PRIORITY_HIGH, TIM.TYPES.MSG_PRIORITY_NORMAL（默认）, TIM.TYPES.MSG_PRIORITY_LOW, TIM.TYPES.MSG_PRIORITY_LOWEST
       * priority: TIM.TYPES.MSG_PRIORITY_NORMAL,
       */
      payload: {
        file: audio,
      },
    })
  }

  /**
   *
   * @param {object} message 要撤回的消息
   * @returns
   */
  revokeMessage(message) {
    return new Promise((resolve, reject) => {
      this.tim
        .revokeMessage(message)
        .then((res) => {
          resolve(res)
        })
        .catch((err) => {
          reject(err)
        })
    })
  }

  sendMessage(message, to) {
    return this.sendIvrConfirmMessage(to)
      .then(() => {})
      .catch(() => {
        return Promise.resolve()
      })
      .then(() => {
        return this.tim.sendMessage(message)
      })
  }
  reSendMessage(message) {
    return this.tim.resendMessage(message)
  }
  /**
   * @description 获取会话列表
   */
  getConversationList() {
    return this.tim.getConversationList()
  }
  setMessageRead(to) {
    this.tim
      .setMessageRead({ conversationID: 'C2C' + to })
      .then()
      .catch(() => {})
  }
}
