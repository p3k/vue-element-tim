import http from '@/utils/http'

export default class Common {
  // 上传
  static uploadFile(data) {
    return http.post('/common/upLoad/upLoadFile', data)
  }
  // 常见问题列表
  static getQuestion(data) {
    return http.post('/reception/tenantQuestion/queryList', data)
  }

  // 是否评价
  static saveRepeatSubmitEvaluates(data) {
    return http.post('/base/evaluates/saveRepeatSubmitEvaluates', data)
  }

  // 客户评价标签
  static getEvaluatesTag(data) {
    return http.post('/base/evaluatesTag/queryList', data)
  }

  // 评价
  static submitEvaluates(data) {
    return http.post('/base/evaluates/saveEvaluates', data)
  }

  // 微信扫码登录
  static wxLogin(data) {
    return http.post('/wxCallbackAndLogin', data)
  }
}
