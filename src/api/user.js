import http from '@/utils/http'

export default class User {
  // 腾讯IM签名
  static getUserSig(data) {
    return http.post('/im/sign/getUserSig', data)
  }
  // 信息
  static getUserInfo(data) {
    return http.post('/reception/tenantReceptionSetting/initWebImInfo', data)
  }
}
