const path = require('path')
const resolve = (dir) => path.join(__dirname, dir)
const webpack = require('webpack')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer')
  .BundleAnalyzerPlugin
// const CompressionWebpackPlugin = require('compression-webpack-plugin')
// const productionGzipExtensions = /\.(js|css|json|txt|html|ico|svg)(\?.*)?$/i

const IS_PROD = ['production', 'prod'].includes(process.env.NODE_ENV)
module.exports = {
  publicPath: IS_PROD ? './' : '/', // 根路径
  outputDir: 'dist', // 构建输出目录
  productionSourceMap: !IS_PROD, // 是否生成map文件
  css: {
    requireModuleExtension: true,
    sourceMap: false,
    loaderOptions: {
      css: {
        // 注意：以下配置在 Vue CLI v4 与 v3 之间存在差异。
        // Vue CLI v3 用户可参考 css-loader v1 文档
        // https://github.com/webpack-contrib/css-loader/tree/v1.0.1
        modules: {
          localIdentName: '[name]-[hash]',
        },
        localsConvention: 'camelCaseOnly',
      },
      sass: {
        prependData: `@import "~@/assets/sass/variables.sass"`,
      },
      /**  在配置 `prependData` 选项的时候
          `scss` 语法会要求语句结尾必须有分号，`sass` 则要求必须没有分号
          在这种情况下，我们可以使用 `scss` 选项，对 `scss` 语法进行单独配置
      */
      scss: {
        // 全局scss样式
        prependData: `@import "~@/assets/scss/variables.scss";`,
      },
      postcss: {
        plugins: [
          require('autoprefixer')(),
          // require('postcss-px2rem-exclude')({
          //   rootValue: 75,
          // }),
        ],
      },
    },
  },
  devServer: {
    contentBase: path.join(__dirname, '/'),
    host: '0.0.0.0',
    port: 8080, // 默认8080
    inline: true, //可以监控js变化
    hot: true, //热启动
    open: true,
    proxy: {
      '/api': {
        target: 'http://127.0.0.1:8080/',
        ws: true,
        changeOrigin: true,
        pathRewrite: {
          '^/api': '/',
        },
      },
    },
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
  },
  // set第一个参数：设置的别名，第二个参数：设置的路径
  chainWebpack: (config) => {
    config.resolve.alias
      .set('@', resolve('src'))
      .set('@assets', resolve('src/assets'))
    config
      .plugin('ignore')
      .use(new webpack.ContextReplacementPlugin(/moment[/\\]locale$/, /zh-cn$/))
    // 页面标题
    config.plugin('html').tap((args) => {
      args[0].title = '在线客服'
      return args
    })
    if (IS_PROD) {
      // 压缩图片
      config.module
        .rule('images')
        .test(/\.(png|jpe?g|gif|svg)(\?.*)?$/)
        .use('image-webpack-loader')
        .loader('image-webpack-loader')
        .options({
          mozjpeg: { progressive: true, quality: 65 },
          optipng: { enabled: false },
          pngquant: { quality: [0.65, 0.9], speed: 4 },
          gifsicle: { interlaced: false },
        })
      // 打包分析
      config.plugin('webpack-report').use(BundleAnalyzerPlugin, [
        {
          analyzerMode: 'static',
        },
      ])
    }
  },
}
